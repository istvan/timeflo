# The TimeFlo Project: Design
*Matthew Istvan 2021*

## Introduction

This document is the design details of my TimeFlo application. TimeFlo is an application that helps users manage their time and tasks efficiently by moderating the amount of time spent on each task. Moderated time intervals for tasks interspersed with breaks helps keep the user more productive. This document will describe the architecture of the TimeFlo application as well as software design details(objects and methods).

## Architecture

*Describe the overall architecture of the proposed
implementation.*
TimeFlo will be implemented in an object oriented fashion with a GUI interaction. The main object will be the TimeFlo object, accessed by main().

### Objects

#### TimeFlo

##### Data Members

- Array of Strings

- Integers for Timer Values

- Sound Object for alarm noise

- Scanner object for input

##### Functionality/Methods

- timeFlo(): default constructor, loads timer values if settings.txt exists

- taskMenu(): displays GUI menu to manipulate the task list(String[])

- addToList(): method to add new tasks to task list

- timerMenu(): displays GUI menu to manipulate the timer values(ints)

- begin(): uses the timer() method and iterates through the TimeFlo process(breaks/tasks)

- timer(): timer for individual intervals of the TimeFlo process(breaks/tasks)

- saveSettings(): saves timer settings to settings.txt
#### Sound

##### Data Members

- Clip for audio

- AudioInputStream to access alarm.wav resource

- String for filepath

##### Functionality/Methods

- sound(): default constructor, loads alarm.wav, will throw an exception on failure to load

- play(): plays the alarm noise

###### Implementation Risks of OOD

The implementation of a simple program as an object oriented design may cost extra coding hours as opposed to a different design methodology.

### Design

Main() will offer options to access and change the task list and timer values. If a task list is loaded, the user may begin the TimeFlo process.

![Design Diagram](docs/design.PNG)
