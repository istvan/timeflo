# The TimeFlo Project: Requirements Document
*Matthew Istvan 2021*

## Introduction

This document will describe the TimeFlo application's functionality and responsibilities. Below you will find a description at the high level of TimeFlo's general applications, who the application is designed for, and specific functional(and extra-functional) requirements. To illustrate TimeFlo's applications there are user cases included which describe what a user may experience when using TimeFlo.

### Purpose and Scope

The purpose of this document is to describe TimeFlo's general functionality and applications for development and informational purposes. 

### Target Audience

This document is for anyone involved in the development or maintenance of TimeFlo, customers/users should refer to the README for instructions on using the application.

### Terms and Definitions

*GUI*
Graphical User Interface

## Product Overview

TimeFlo is an application which allows a user to create a task list for themselves and then helps then manage their productivity for the set of tasks by creating custom timed intervals of tasks and breaks. The user is alerted to the transition between tasks and breaks by visual and audible cues. A user is someone who will use the application for it's intended purposes of maximizing productivity for a task or set of tasks. A stakeholder for this application is anyone involved in or affected by the creation, distribution, management, or functionality of this application. In this section there has been a use case included to illustrate a typical user's experience when using TimeFlo.

### Users and Stakeholders

Within this section there will be a breakdown of the stakeholders for TimeFlo and a user case. Stakeholders included are: the initial developer, the auditor, the end-user, and the maintenance developer.

#### *Initial Developer*

The initial developer is responsible for the creation of TimeFlo from scratch. The initial developer will use this document to understand exactly what TimeFlo's purpose is, it's functionality, and it's responsiblities and requirements.

#### *Auditor*

The auditor is responsible for surveying the code and the TimeFlo application to verify it's functionality and whether it's met it's goals and purposes. The auditor will use this document to cross examine the final product.

#### *End User*

The end-user is the customer who will be using this product. The end-user's satisfaction with this product will be dependent on whether this functionality and requirements described in this document have been met.

#### *Maintenance Developer*

The maintenance developer is the developer that will be responsible for maintaining the code after the product has been released. The maintenance developer will use this document to understand the fundamental functionality of TimeFlo and what TimeFlo will continue to require over time as the product takes on additional functionality or undergoes further development.

### Use cases

The following is a fictional use case. The purpose of this use case is to describe how a user will interact with TimeFlo, what their intentions and wants are, and how the application will be used in a real life scenario. The goal of this use case is to help developers understand how a user will actually be using the application so they can develop a product that will satisfy it's high level purposes and consistently provide it's functionality without falling short of it's requirements.

#### *Use Case: Ted Smith*

- Name: Ted Smith
- Occupation: Part-Time Student, Full-Time Retail Cashier
- Age: 27
- Description: Married father of two, works full time Monday to Friday, re-educating part-time via remote classes

Ted Smith sits down after a full day at his retail store to check and return emails, pay his bills and manage his bank account, work on his classwork, and make dinner. Ted opens the TimeFlo application to help him manage his productivity over his array of tasks. Ted selects to create a new task list for his varying tasks for the day and begins entering in his tasks. He enters in "Check/Return Emails" and hits enter, "Manage Bills/Expenses" and hits enter, "Work on ClassW" and his 7 year old son jumps on his legs from his blind spot, causing him to accidently hit enter too early. Ted picks up his son and places him down, selects to "Delete Task List" and begins re-entering his tasks. This time Ted fully enters all his tasks successfully. Ted now selects to "Begin Timing". The application gives him a prompt to hit enter when he's ready, he hits enter, the timer begins for "Check/Return Emails", and Ted switches over to his web browser. Ted did not select any times so the system used the default time settings of 25(task)/5(break)/15(long break). After 25 minutes TimeFlo displays a message declaring it is break time and plays an audible tone. Ted switches over to the TimeFlo application, hits enter to acknowledge the transition, and gets up from his desk. While in the kitchen grabbing water, Ted hears TimeFlo's alert noise and sits back down, acknowledges the alert and begins on his next task "Manage Bills/Expenses". Ted works through his bills and expenses for the duration of the 25 minutes and proceeds to his break. After coming back from his break he returns to begins "Work on Classwork". 15 minutes into working on his classwork, he hears a call from his wife from another room. Ted switches to the TimeFlo application, hits enter to pause the task and gets up from his desk. Some time later, Ted returns to his desk and hits space again on the TimeFlo application to resume the timer. Ted finishes his "Work on Classwork" task, proceeds to his break, which is a and returns to the "Make Dinner" task. Ted gets up from his desk for this task and heads into the kitchen to begin making dinner. After 25 minutes Ted hears the TimeFlo alert at his desk, but he is not done cooking dinner. He continues to make dinner for another 10 minutes then returns to his desk to acknowledge the alert. Ted feels that his task times were too short, so he goes into the menu and changes his tasks timer to 35 minutes for the next time he uses TimeFlo. Now he is done with his tasks and exits TimeFlo.

## Functional Requirements

This section contains the functional requirements of TimeFlo. The functional requirements are essentially what the program will be *doing*. The functional requirements also describe what TimeFlo is responsible for, what data it needs to keep and track.

### *GUI*

TimeFlo must display a GUI to the user to allow them to interact with the program. The GUI will offer options:

Main-Menu:

- A) Task List
- B) Timer Settings
- C) Begin Timing
- Q) Quit

Sub-Menus:

*A) Task List*
- A) Create New Task List
- B) Display Task List
- C) Delete Task List
- D) Return To Previous Menu

*B) Timer Settings*
- A) Display Timer Values
- B) Set Timer Values
- C) Return To Previous Menu

*C) Begin Timing*
If the user selects "Begin Timing", the timing will begin if there is a task list instatiated.

### *Task List*

TimeFlo must allow the user to create a task list. This task list will be a list of sentences that name or describe the tasks using ASCII characters.

### *Timer Settings*

TimeFlo must allow the user to select task and break times, or use pre-set values. These times will be selected in whole minutes(ie: integers) ranging from 1 to 60.

### *Invalid Input Detection*

TimeFlo must recognize invalid inputs during the task creation, timer settings, or GUI interaction and notify the user of incorrect input.

### *Task and Break Timing*

Once a task list has been instatiated and task/break times are set, TimeFlo will allow the user to begin timing their tasks and breaks using the system time on their machine.

### *Long Breaks/Short Breaks*

TimeFlo will have time settings for both long and short breaks. There will be short breaks between every task execpt every 4 tasks where there will be a long break.

### *Visual Cue/Audible Cue*

TimeFlo must display a visual cue and audible sound when a task or break's time has ended and allow the user to acknowledge the end of the task/break before moving onto the next task/break.

### *Pause and Resume*

TimeFlo must allow the user to interrupt and pause a task or break and resume it later as long as the program stays running.

### *Saving User Selections*

When exitting, TimeFlo needs to store the values of the user selected task and break times in a file for the next session.

## Extra-functional Requirements

This section will describe the requirements of TimeFlo that are not explicitly functional. These are general systematic requirements that the program overall is responsible for during normal(ie: on a functional system) operation. 

### *Windows 10 Compatibility*

TimeFlo should work on Windows 10 systems without losing functionality.

### *Crash Vulnerability*

TimeFlo should never crash as long as a system is running properly

