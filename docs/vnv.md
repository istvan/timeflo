# The TimeFlo Project: Validation and Verification
*Matthew Istvan 2021*

## Introduction

This Verification and Validation document will describe the methods used to check the TimeFlo application for proper functionality and bugs. In this document you will find descriptions of the procedures for V&V of the TimeFlo application, methodology, scope of testing, 

### Purpose and Scope

This document describes the methods taken to ensure the quality and functionality of the TimeFlo application. 

### Target Audience

This document is for developers of the TimeFlo application as well as auditors of the source code.

### Terms and Definitions

- Method: A function, a piece of the software responsible for part of the overall system functionality

## V&amp;V Plan Description

To test the TimeFlo application for proper functionality towards the requirements outline in the Requirements Document, several tests will be run during Stage 1 Development and after Stage 1 Development is finished. Three main types of testing will be included: unit testing, system testing, and code inspection.

## Test Plan Description

The test plan for the TimeFlo is described below. Unit testing will be conducted on the most important methods towards the pverall functionality. System testing will be conducted to validate functionality after the units have been verified. Code inspection should be conducted regularly, as well as before and after unit/system testing. 

### Scope Of Testing

To test the TimeFlo application, two types of main aspects will be tested: 

- average/expected value input testing
- basic erroneous situation/input testing.

Some testing will not be included due to time constraints, such as:

- very erroneous input testing
- compatiblity testing beyond requirements

To test the input and timer functionality, many runs of varying task list sizes will be conducted, and the extreme ends of the timer functionality will be tested(based on the assumption that if the extremes are funcional, so too should the medial values).

### Release Criteria

On release, TimeFlo must be able to accept timer values from 1 to 60 minutes for each type of interval(task/break/long break). TimeFlo must also be able to accept a task list up to 20 items without losing functionality.

## Unit Testing

TimeFlo's main functionality lies in the creation of the task list and the timer. The two methods that are responsible for the majority of this functionality will be tested to ensure validity and lack of errors.

- addToList()

- timer()

### Strategy

To test these units manual input and testing will be conducted via the command line of the IDE. The other units have been excluded due to time constraints, because their functionality is essential to the two tested functions, and therefore must be functioning if the main two are functioning, OR because their dependant on libraries outside of my technical experience(ie: extensive audio bugs).
### Test Cases
#### addToList()
In testing addToList(), list sizes of 1-20 will be created and tested for validity via inspection by debug menu in the IDE as well as tested during an execution of the application.

#### timer()
In testing the timer() method, different values from from 1-5 will be tested as well as 55-60. To test the functionality, the timer must be tested by execution of the application, so several trials of the application will be conducted will small list sizes and the appropriate timer values.
## System Testing

System testing is overall testing of the entire application. To test the overall system for full functionality, several manual test cases were created and described below.

### Test Cases

- Settings.txt Missing
    - Deleted the settings.txt from the source files and tested if the application would load proper default values and run properly, as well as create a settings.txt on exit.
    - Operated as expected.
- 20 Item Task List/Custom Timer Settings
    - Created a long task list(20 items), and changed the timer values. Tested to see if the proper timer values were used for task/break/long-break, and saved on exit.
    - Operated as expected.

## Inspection

Code inspection will be conducted on each unit prior to unit testing. Inspection will be conducted to verify the code is written as intended with no typos or syntax errors, prior to testing the entire unit for erroneous functionality. Before commitments/pushes to the main branch, code will be inspected for unnecessary lines, proper values(if values have been changed during unit testing), and readability.
