# The TimeFlo Project: Project Plan
*Matthew Istvan 2021*


## Resources

Acer Laptop
Intellij IDEA
GitLab

## Work Breakdown


### Requirements Ellicitation
Time Estimate: 1 Hour

Prerequisites: N/A

Dependents: Requirements Documentation

### Requirements Documentation
Time Estimate: 2 Hours

Prerequisites: Requirements Ellicitation

Dependents: Detail Drafting, Architecture Drafting

### Architecture Drafting
Time Estimate: 2 Hours

Prerequisites: Requirements Documentation

Dependents: Detail Drafting, Detail Documentation

### Detail Drafting
Time Estimate: 1 Hour

Prerequisites: Architecture Drafting

Dependents: Detail Documentation

### Detail Documentation
Time Estimate: 1 Hour

Prerequisites: Detail Drafting, Architecture Drafting

Dependents: Stage 1 Development

### Stage 1 Development
Time Estimate: 8 Hours

Prerequisites: Requirements Documentation, Detail Documentation

Dependents: Verification and Validation Testing, Verification and Validation Documentation

### Verification and Validation Documentation
Time Estimate: 1 Hour

Prequisites: Requirements Documentation, Detail Documentation, Stage 1 Development

Dependents: Verification and Validation Testing

### Verification and Validation Testing
Time Estimate: 1 Hour

Prerequisites: Stage 1 Development, Verification and Validation Documentation

Dependents: Stage 2 Development, Verification and Validation Stage 2

## Possible Second-Phase Tasks

### Stage 2 Development
Time Estimate: 2 Hours

Prerequisites: Stage 1 Development, Verification and Validation Testing

Dependents: Verification and Validation 2

### Stage 2 Verification and Validation
Time Estimate: 1 Hour

Prerequisites: Stage 2 Development

Dependents: Next Stage Development/V&V

### Schedule

- Requirements Documentation(10/20/21)

- Design/Detail Documentation(10/27/21)

- Stage 1 Development(10/30/21)

- Verification and Validation Documentation(11/6/21)

- Project Completion(11/9/21)


## Milestones and Deliverables

The following are significant milestones for the TimeFlo project. They include documentation, development, testing, and project completion targets.

### *Requirements Submission*

Requirements Submission is the completion and archival of the requirements document. To complete the requirements document, requirements ellicitation must be completed, then the requirements must be written and user cases/stakeholder info/etc. must be created and then compiled into proper format.

### *Planning Submission*

Planning Submission is the completion and archival of the planning document(which you're reading now). To write the planning document pre-planning research and possible drafting must be done.

### *Detail Submission*

Detail Submission is the completion and archival of the detail document. To complete the detail document the requirements document must be completed as well as a draft of the software architecture. 

### *Stage 1 Development*

Stage 1 Development is the completion of an initial version of the TimeFlo software. To complete Stage 1 Development a detail and requirements document must be created. 

### *Verification and Validation*

Verification and validation is the planning, documentation, and testing that will be done on the TimeFlo software. To proceed with the verification and validation the requirements/detail documentation must be completed, as well as Stage 1 Development.
