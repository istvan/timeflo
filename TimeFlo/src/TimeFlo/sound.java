//Matthew Istvan 2021
//Sound class, contains and manages the audio for the alarm
package TimeFlo;
import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Clip;
import javax.sound.sampled.UnsupportedAudioFileException;
//Sound object reads in an audio file an contains it in a clip, responsible for playing that clip, in this case an alarm for the timer
public class sound {
    Clip clip;
    AudioInputStream audioStream;
    String filePath = "TimeFlo/alarm.wav";
    //constructor, throws an exception for invalid filepaths, invalid files, etc.
    public sound()throws UnsupportedAudioFileException, IOException, LineUnavailableException{
        audioStream = AudioSystem.getAudioInputStream(new File(filePath));
        clip = AudioSystem.getClip();
        clip.open(audioStream);
    }
    //plays the audiofile and waits for 5 seconds so the audio doesn't get cut-off
    public void play() {
        clip.setMicrosecondPosition(0);
        clip.start();
        try {
            TimeUnit.SECONDS.sleep(5);
        }
        catch(Exception e){
            System.out.println("Sound interrupted");
        }
    }
}
