//Matthew Istvan 2021
//Timeflo Class, manages tasklist and timer
package TimeFlo;

import java.io.*;
import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

//timeFlo object has and manages a timer and a list of tasks
public class timeFlo {
    String[] list;
    int breakTime, longBreakTime, taskTime;
    Scanner input;
    sound Alarm;

    //constructor
    public timeFlo() {
        Exception InvalidSettingsFile = new Exception("Invalid Settings File");
        try {
            Scanner fileSettings = new Scanner(new File("settings.txt"));
            taskTime = Integer.parseInt(fileSettings.nextLine());
            breakTime = Integer.parseInt(fileSettings.nextLine());
            longBreakTime = Integer.parseInt(fileSettings.nextLine());
            fileSettings.close();
            if (taskTime < 0 || taskTime > 60 || breakTime < 0 || breakTime > 60 || longBreakTime < 0 || longBreakTime > 60) {
                throw InvalidSettingsFile;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            taskTime = 25;
            breakTime = 5;
            longBreakTime = 15;
        }
        list = null;
        input = new Scanner(System.in);
        try {
            Alarm = new sound();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    //task menu to create and delete a task list
    public void taskMenu() {
        char c;
        printList();
        while (true) {
            System.out.println("A) Add To List");
            System.out.println("B) Delete List");
            System.out.println("C) Print Current List");
            System.out.println("Q) Return to Previous Menu");
            c = input.nextLine().charAt(0);
            switch (c) {
                case ('a'):
                    addToList(input);
                    break;
                case ('b'):
                    list = null;
                    break;
                case ('c'):
                    printList();
                    break;
                case ('q'):
                    return;
                default:
                    System.out.println("Invalid Selection");
            }
        }
    }

    //functions to create the task list, adding tasks one after another in a loop till broken by the user
    private void addToList(Scanner input) {
        System.out.println("Enter Tasks, when finished type \"Quit\" or \"Done\"");
        String buffer;
        while (true) {
            buffer = input.nextLine();
            if (buffer.toLowerCase().equals("done") || buffer.toLowerCase().equals("quit"))
                return;
            addToList(buffer);
        }
    }

    private void addToList(String toAdd) {
        String[] newList;
        if (list == null) {
            newList = new String[1];
            newList[0] = toAdd;
        } else {
            newList = new String[list.length + 1];
            for (int i = 0; i < list.length; i++)
                newList[i] = list[i];
            newList[newList.length - 1] = toAdd;
        }
        list = newList;
    }

    //prints the list
    private void printList() {
        System.out.println("Current List: ");
        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                System.out.println(i + 1 + ": " + list[i]);
            }
        } else {
            System.out.println("Empty");
        }
    }

    //timer menu to change timer values
    public void timerMenu() {
        char c;
        System.out.println("Task Time: " + taskTime);
        System.out.println("Break Time: " + breakTime);
        System.out.println("Long Break Time: " + longBreakTime);
        while (true) {
            System.out.println("A)Change Task Time");
            System.out.println("B)Change Break Time");
            System.out.println("C)Change Long Break Time");
            System.out.println("Q)Quit");
            c = input.nextLine().toLowerCase().charAt(0);
            switch (c) {
                case ('a'):
                    taskTime = changeValue("Task");
                    break;
                case ('b'):
                    breakTime = changeValue("Break");
                    break;
                case ('c'):
                    longBreakTime = changeValue("Long Break");
                    break;
                case ('q'):
                    return;
                default:
                    System.out.println("Invalid Selection");
            }
        }
    }

    //changes the timer values
    private int changeValue(String type) {
        String buffer;
        int i;
        while (true) {
            System.out.print("New " + type + " Time: ");
            buffer = input.nextLine();
            try {
                i = Integer.parseInt(buffer);
                if (i < 1 || i > 60) {
                    System.out.println("Invalid value(must be 1 to 60)");
                } else {
                    System.out.println(type + " Time set to: " + i);
                    return i;
                }
            } catch (Exception e) {
                System.out.println("Invalid Input, please enter an integer(1 to 60)");
            }
        }
    }

    //begins the timer, iterates through the tasks, then returns to main
    public void begin() {
        if (list == null) {
            System.out.println("There are no tasks");
            return;
        }
        for (int i = 1; i < list.length + 1; i++) {
            timer(taskTime, list[i - 1]);
            if (i % 4 == 0)
                timer(longBreakTime, "a long break");
            else
                timer(breakTime, "a short break");
        }
        System.out.println("Tasks Finished!");
    }

    //timer function
    //Timer interrupt based on(NOT copied) code from:
    //jrtc27 (2012) Pause Loop(Version 1.0) [Source code].https://stackoverflow.com/questions/13105122/loop-in-java-until-the-users-pushes-enter
    private void timer(int time, String task) {
        System.out.println("Now begin " + task + "(" + time + " minute(s))" +
                "");
        long timeStart = System.currentTimeMillis();
        long timeEnd = timeStart + (60000 * time);
        long currentTime;
        String buffer;
        while (true) {
            currentTime = (timeEnd - System.currentTimeMillis());
            System.out.print((int)(currentTime/60000)+":");
            System.out.print((currentTime-((int)(currentTime/60000))*60000)/1000 + "\r");
            try {
                if (System.in.available() != 0) {
                    System.out.println("Paused");
                    System.out.print((int) (currentTime / 60000) + ":");
                    System.out.println((currentTime - ((int) (currentTime / 60000)) * 60000) / 1000 + " Left");
                    buffer = input.nextLine();
                    while (System.in.available() == 0) {
                    }
                    buffer = input.nextLine();
                    System.out.println("Resumed");
                    timeEnd = System.currentTimeMillis() + currentTime;
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            if (System.currentTimeMillis() > timeEnd) {
                System.out.println(task + " finished!");
                Alarm.play();
                System.out.println("Type anything and enter to continue");
                buffer = input.nextLine();
                return;
            }
        }
    }

    //saves timer settings to settings.txt
    public void saveSettings() {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("settings.txt"));
            out.write(taskTime + "\n");
            out.write(breakTime + "\n");
            out.write(longBreakTime + "\n");
            out.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
