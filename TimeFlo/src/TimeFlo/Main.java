//Matthew Istvan 2021
//TimeFlo application main function, displays main menu gui
package TimeFlo;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        timeFlo timer = new timeFlo();
        while(true){
            System.out.println("A) Create New Tasklist");
            System.out.println("B) Set timer settings");
            System.out.println("C) Begin Timer");
            System.out.println("Q) Quit");
            try {
                char c = input.nextLine().toLowerCase().charAt(0);
                switch (c) {
                    case ('a'):
                        timer.taskMenu();
                        break;
                    case ('b'):
                        timer.timerMenu();
                        break;
                    case ('c'):
                        timer.begin();
                        break;
                    case ('q'):
                        timer.saveSettings();
                        return;
                    default:
                        System.out.println("Invalid Selection");
                }
            }
            catch(Exception e){
                System.out.println(e.getMessage());
            }

        }
    }
}




