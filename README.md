# TimeFlo
Copyright &copy; 2021 *Matthew Istvan*

TimeFlo is an implementation of a
[Pomodoro&reg;](https://en.wikipedia.org/wiki/Pomodoro_Technique)-like
timer for breaking out of flow state.

TimeFlo is an application that offers the ability to time your tasks for maximum effieciency. TimeFlo allows you to create a tasklist and time yourself with customizable timer settings. An alarm will go off at the end of each interval signalling a transition to the next task/break. Pause/Resume options are available, just press enter to pause the timer and enter again to resume.

## Status and Roadmap


* [x] Requirements complete.
* [x] Project plan complete.
* [x] Design complete.
* [x] Implementation complete.
* [x] Validation complete.

## Build and Run

From GitBASH, enter the TimeFlo/src/TimeFlo directory and type "javac *.java" to compile all the source files

Exit the directory(cd ..) and run the application with "java TimeFlo.Main"

*Note: Please let me know ASAP if the soundfile doesn't load properly, it needs a different filepath between IntelliJ and Git Bash, it's currently configurated for Git Bash*

## Development Docs

Development documentation is available for TimeFlo, including:

* [Requirements Specification](docs/requirements.md)
* [Project Plan](docs/plan.md)
* [Design Doc](docs/design.md)
* [V&amp;V Report](docs/vnv.md)
